# String

Provides a developer friendly way for managing strings in your project.
Instead of hard-coding the actual string all over your codebase, this
module enables you to add some unique string identifiers.

## Sample usage

| Current (Core) | String |
|---|---|
| `t('Dashboard')` | `t('dashboard.title')`
| `t('My Dashboard')` | `t('user_dashboard.title')`
| `t('Welcome @name!', ['@name' => $name])` | `t('dashboard.welcome_message.title', ['@name' => $name])`
| `format_plural($count, '1 item', '@count items')` | `format_plural($count, 'search_result.item_count', 'search_result.item_count')`

## Advantages

- Makes your codebase independent of translation workflow.
- No need to update your codebase to edit strings
- Editing a text in original language won't invalidate the translation in
  other language.
- More intuitive use of translation context.
- Instead of reference to codebase and source files, strings are managed via
  unique human friendly string "ids".

## Defining strings

Before you can start using "identifiers", we need to tell Drupal about it. For
this, after enabling the module, define your strings in a custom module via
`string` yaml plugin. This is similar to creating routes via routing.yml plugin.

Let say you have a module named `foo`, then create a file in the root of your
module folder called `foo.string.yml`. Each entry would represent a unique
string. Please refer to sample configuration file with ample comment.

Strings are logically grouped using `.` (a period) in their identifier.

Example `dashboard.welcome_message.short` would be part of following groups,
- `dashboard`
- `dashboard.welcome_message`

The top level group is called `namespace`. Usually this would be the module's
machine name, but not necessarily. The identifier thus follows a format
`namespace.key`. Where `key` can also have its own grouping.

NOTE: String IDs MUST have a `namespace` and a `key`.

```yml
# Demo: String with a placeholder.
dashboard.welcome_message.short:
  default: "Hello @name!"
# Demo: Another string with placeholder.
dashboard.welcome_message.long:
  default: "Hello @name! Welcome back, good to see you again."
# Demo: Plural strings.
search.result.items_count:
  default: "@count item found"
  default_plural: "@count items found"
# Demo: Expose strings from contrib module.
# Following is a string which is actually used in a contrib module "Group".
drupal.contrib.groups.label:
  default: "Groups"
  msgid: "Groups"
# Demo: Expose strings from core.
# Following is a string from core.
drupal.core.module.extend.label:
  default: "Extend"
  msgid: "Extend"
# Demo: String Override.
# @TODO:
# Following is a way you could override an existing string.
# Much like string overrides. This allows us to not add
# another English language to replace existing string.
drupal.core.node.content.label:
  default: "Pages"
  msgid: "Content"
# A string with context.
# However it really doesn't make any sense.
dashboard.greetings.short:
  default: 'Sup @name!'
  msgid: 'Hi @name!'
  msgctxt: 'short_greeting'
# A string with context.
# However it really doesn't make any sense.
drupal.core.node.search_result_summary:
  default: 'One item found.'
  default_plural: '@count items found.'
  msgid: '@count item found'
  msgid_plural: '@count items found'
  # You may also add some comments for each string.
  comments:
    - 'Comment line 1'
    - 'Comment line 2'
```

After clearing the cache, you can start using the identifier in your
translation function calls. E.g. `t('dashboard.welcome_message.short')`.

## Admin interface

### Exporting translation source file (i.e. POT files)

Export form is located at `/admin/config/regional/translate/string-export`.
It can also be accessed through,

>> Configuration -> Regional and language -> User interface translation ->
String Export.

You may have to provide `manage string export` permission to the users
for doing this.

### Importing translation file (i.e. PO files)

You can use the interface provided by locale module to import custom
translation files.
This is located at `/admin/config/regional/translate/import`
It can also be accessed through,

>> Configuration -> Regional and language -> User interface translation ->
Import.

## Compatible with

- Strings exposed are available to be translated via Interface translation
- Translation strings can be imported using pot files via core interface.

## Sub Modules

### string_demo

Enable `string_demo` to see how definitions in `string_demo.string.yml`
gets converted to string item.

You can also check few test cases displayed at `/admin/config/string_demo/test`

### string_tmgmt

Enable `string_tmgmt` to integrate with `tmgmt` module.
This exposes an interface to exclusively browse for strings exposed
via string plugin.

## References

- [The Format of PO Files](https://www.gnu.org/software/gettext/manual/html_node/PO-Files.html)
-
