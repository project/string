<?php

namespace Drupal\string\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\FileDownloadController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for export options.
 */
class StringSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'string.settings';

  /**
   * Drupal\language\ConfigurableLanguageManagerInterface definition.
   *
   * @var \Drupal\language\ConfigurableLanguageManager
   */
  protected $languageManager;

  /**
   * Drupal\language\StringSettingsFormulaInterface definition.
   *
   * @var \Drupal\string\Services\StringPluralFormulaInterface
   */
  protected $stringPluralFormula;

  /**
   * Drupal\Core\Plugin\Context\ContextProviderInterface definition.
   *
   * @var \Drupal\Core\Plugin\Context\ContextProviderInterface
   */
  protected $languageCurrentLanguageContext;

  /**
   * Drupal\string\StringManager definition.
   *
   * @var \Drupal\string\StringManager
   */
  protected $string;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannelDefault;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->languageManager = $container->get('language_manager');
    $instance->stringPluralFormula = $container->get('string.plural_formula');
    $instance->languageCurrentLanguageContext = $container->get('language.current_language_context');
    $instance->loggerChannelDefault = $container->get('logger.channel.default');
    $instance->string = $container->get('plugin.manager.string');
    $instance->fileDownloadController = FileDownloadController::create($container);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'string_plural_form';
  }

  /**
   * Get active languages in the system.
   */
  public function getLanguages(): array {
    $active_languages = $this->languageManager->getLanguages();
    return $active_languages;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form['lang_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Language'),
        $this->t('Active Plural Rule'),
        $this->t('Overridden Plural Rule'),
      ],
      '#empty' => $this->t('No Languages found'),
    ];
    $active_languages = $this->getLanguages();
    foreach ($active_languages as $language_id => $value) {
      $form['lang_table'][$language_id][0] = [
        '#markup' => $language_id,
      ];
      $default_rule = $this->stringPluralFormula->getRuleById($language_id);
      $fallback_rule = $this->stringPluralFormula->getDefaultPluralFormula($language_id);
      $form['lang_table'][$language_id][1] = [
        '#markup' => $default_rule,
      ];
      $form['lang_table'][$language_id][2] = [
        '#type' => 'textfield',
        '#default_value' => $config->get("language_rule.$language_id") ?? '',
        '#size' => 100,
        '#attributes' => [
          'placeholder' => $fallback_rule,
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  // phpcs:disable Generic.CodeAnalysis.UselessOverridingMethod

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $language_rule = $form_state->getValues('lang_table');
    foreach ($this->getLanguages() as $language_id => $support_language_value) {
      $language_rule_by_id = $language_rule['lang_table'][$language_id][2];
      $this
        ->configFactory
        ->getEditable(static::SETTINGS)
        ->set('language_rule.' . $language_id, $language_rule_by_id)
        ->save();
    }

  }

}
