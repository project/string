<?php

namespace Drupal\string\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\string\StringManager;
use Drupal\system\FileDownloadController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Form for export options.
 */
class ExportForm extends FormBase {
  use StringTranslationTrait;

  /**
   * Drupal\language\ConfigurableLanguageManagerInterface definition.
   *
   * @var \Drupal\language\ConfigurableLanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Drupal\Core\Plugin\Context\ContextProviderInterface definition.
   *
   * @var \Drupal\Core\Plugin\Context\ContextProviderInterface
   */
  protected $languageCurrentLanguageContext;

  /**
   * Drupal\string\StringManager definition.
   *
   * @var \Drupal\string\StringManager
   */
  protected $string;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannelDefault;

  /**
   * Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Datetime service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $dateTime;


  /**
   * File repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->languageManager = $container->get('language_manager');
    $instance->languageCurrentLanguageContext = $container->get('language.current_language_context');
    $instance->loggerChannelDefault = $container->get('logger.channel.default');
    $instance->string = $container->get('plugin.manager.string');
    $instance->configFactory = $container->get('config.factory');
    $instance->fileDownloadController = FileDownloadController::create($container);
    $instance->dateTime = $container->get('datetime.time');
    $instance->fileRepository = $container->get('file.repository');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'string_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('string.settings');
    $form['strings'] = [
      '#type' => 'tableselect',
      '#header' => [],
      '#options' => [],
      '#title' => $this->t('Table'),
    ];
    $form['show_export'] = [
      '#type' => 'checkbox',
      '#default_value' => 0,
      '#title' => $this->t('Export Pot File to machine path?'),
    ];
    $form['destination'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Interface directory'),
      '#default_value' => empty($config->get('destination.path')) ? 'public://string.pot' : $config->get('destination.path'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#description' => $this->t('A local file system path where "POT" file will be stored.'),
      '#states' => [
        'visible' => [
          ':input[name="show_export"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['actions']['download'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download'),
      '#submit' => [[$this, 'submitDownloadForm']],
    ];
    $form['actions']['export'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
      '#submit' => [[$this, 'submitExportForm']],
      '#states' => [
        'visible' => [
          ':input[name="show_export"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->submitDownloadForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitExportForm(array &$form, FormStateInterface $form_state) {
    $destination = $form_state->getValue('destination');
    $this->exportFile($form_state, $destination);
  }

  /**
   * {@inheritdoc}
   */
  public function submitDownloadForm(array &$form, FormStateInterface $form_state) {
    $this->downloadFile($form_state);
  }

  /**
   * Build pot file.
   *
   * @return string[]
   *   Build the Pot file to have necessary items and be more user friendly.
   */
  protected function buildPotFile() {
    $output = $this->getHeader();
    if ($definitions = $this->string->getDefinitions()) {
      foreach ($definitions as $item) {
        $msgid_plural = $this->getStringMessagePluralId($item);
        $output[] = '';
        if (!empty($item[StringManager::COMMENTS])) {
          foreach ($item[StringManager::COMMENTS] as $comment) {
            $output[] = '# ' . $comment;
          }
        }
        $output[] = '#. Following text is used as default if no translation is available';
        $output[] = '#. -----------------------------';
        $output[] = '#. ' . $item[StringManager::DEFAULT_VALUE];
        $output[] = '#. -----------------------------';
        if (isset($item[StringManager::MSG_ID])) {
          $output[] = '#.';
          $output[] = '#.';
          $output[] = '#. Original Drupal text that is being overridden is as follows';
          $output[] = '#. -----------------------------';
          if ($msgid_plural) {
            $output[] = '#. SINGULAR';
            $output[] = '#. ' . $item[StringManager::MSG_ID];
            $output[] = '#. ';
            $output[] = '#. PLURAL';
            $output[] = '#. ' . $item[StringManager::MSG_ID_PLURAL];
          }
          else {
            $output[] = '#. ' . $item[StringManager::MSG_ID];
          }
          $output[] = '#. -----------------------------';
        }
        $output[] = '#: ' . $this->getStringSourceId($item);
        if (isset($item[StringManager::MSG_CONTEXT])) {
          $output[] = 'msgctxt "' . $item[StringManager::MSG_CONTEXT] . '"';
        }
        $output[] = 'msgid "' . $this->getStringMessageId($item) . '"';
        if ($msgid_plural) {
          $output[] = 'msgid_plural "' . $msgid_plural . '"';
          $output[] = 'msgstr[0] ""';
          $output[] = 'msgstr[1] ""';
        }
        else {
          $output[] = 'msgstr ""';
        }
      }
    }
    return $output;
  }

  /**
   * Get message id.
   */
  protected function getStringMessageId($definition) {
    $id = $definition[StringManager::STRING_ID];
    if (isset($definition[StringManager::MSG_ID])) {
      $id = $definition[StringManager::MSG_ID];
    }
    return $id;
  }

  /**
   * Get source id for plural form.
   */
  protected function getStringMessagePluralId($definition) {
    $id = '';
    if (isset($definition[StringManager::MSG_ID_PLURAL])) {
      $id = $definition[StringManager::MSG_ID_PLURAL];
    }
    elseif (isset($definition[StringManager::DEFAULT_VALUE_PLURAL])) {
      $id = $definition[StringManager::STRING_ID];
    }
    return $id;
  }

  /**
   * Get source id.
   */
  protected function getStringSourceId($definition) {
    return $definition[StringManager::STRING_ID];
  }

  /**
   * Get header for pot file.
   *
   * @return string[]
   *   Header as list of string, where each item is single line in pot file.
   */
  protected function getHeader() {
    return [
      '# $Id$',
      '#',
      '# Export generated by string module (Drupal).',
      '# Time ' . $this->dateTime->getRequestTime(),
      '#',
      '#, fuzzy',
      'msgid ""',
      'msgstr ""',
      '"Project-Id-Version: PROJECT VERSION\n"',
      '"POT-Creation-Date: 2020-07-06 11:52+0000\n"',
      '"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"',
      '"Last-Translator: NAME <EMAIL@ADDRESS>\n"',
      '"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"',
      '"MIME-Version: 1.0\n"',
      '"Content-Type: text/plain; charset=utf-8\n"',
      '"Content-Transfer-Encoding: 8bit\n"',
      '"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"',
    ];
  }

  /**
   * Export content to file.
   *
   * @param string $content
   *   Content of file to be saved.
   * @param string $destination
   *   Destination of file to be saved.
   *
   * @return \Drupal\file\FileInterface|false|\PHPUnit\Framework\MockObject\MockObject
   *   File that has been exported.
   */
  protected function validateExportFile(string $content, $destination) {
    $schema = StreamWrapperManager::getScheme($destination);
    if ($schema == 'public') {
      $file = $this->fileRepository->writeData($content, $destination, FileSystemInterface::EXISTS_REPLACE);
      $this->messenger()->addStatus($this->t('The translate items has been export to %destination', [
        '%destination' => $destination,
      ]));
    }
    else {
      $this->messenger()->addError($this->t('Invalid stream wrapper: %destination', ['%destination' => $destination]));
    }

    return $file;
  }

  /**
   * Export content to file and Create file in case it is missing.
   *
   * @param object $form_state
   *   Values from submit method.
   * @param string $destination
   *   Destination where file should be saved.
   */
  protected function exportFile($form_state, $destination) {
    $destination = $form_state->getValue('destination');
    $schema = StreamWrapperManager::getScheme($destination);
    $taget = StreamWrapperManager::getTarget($destination);
    $filename = substr($taget, strrpos($taget, '/') + 1);
    $taget_without_filename = str_replace($filename, '', $taget);
    $output = $this->buildPotFile();
    $this->configFactory->getEditable('string.settings')->set('destination.path', $destination)->save();
    if (file_exists($destination)) {
      $this->validateExportFile(implode("\n", $output), $destination);
    }
    else {
      mkdir($schema . '://' . $taget_without_filename, 0777, TRUE);
      $file = File::create([
        'uid' => 1,
        'filename' => basename($destination),
        'uri' => $destination,
        'status' => 1,
      ]);
      $this->validateExportFile(implode("\n", $output), $destination);
    }
  }

  /**
   * Export file first to hardcoded destination then download the file.
   *
   * @param object $form_state
   *   Values from submit method.
   */
  protected function downloadFile($form_state) {
    $destination = 'public://string.pot';
    $this->exportFile($form_state, $destination);
    $response = new BinaryFileResponse($destination);
    $response->setContentDisposition('attachment', 'string.pot');
    $download = $form_state->setResponse($response);
    return $download;
  }

}
