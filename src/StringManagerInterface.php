<?php

namespace Drupal\string;

/**
 * StringManager class definition.
 */
interface StringManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id);

}
