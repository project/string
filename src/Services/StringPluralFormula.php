<?php

namespace Drupal\string\Services;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Return the rule of the given language id.
 */
class StringPluralFormula implements StringPluralFormulaInterface {

  const SETTINGS = 'string.settings';


  /**
   * Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * StringPluralFormula constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Return a list of all support languages.
   */
  public function getStaticLanguageRules() {
    return [
      'pl' => 'n==1 ? 0 : n%10>=2 and n%10<=4 and (n%100<12 || n%100>14) ? 1 : 2',
      'ar' => 'n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5',
      'ru' => 'n%10==1 ? 0 : n%10==2 ? 1 : n%10==3 ? 1 : n%10==4 ? 1 : 2',
    ];
  }

  /**
   * Get the rule of a language id from the config.
   */
  protected function getRulesFromConfig($lang_code) {
    $config = $this->configFactory->get(static::SETTINGS);
    return $config->get("language_rule.$lang_code") ?: FALSE;
  }

  /**
   * Get default plural formula when nothing is defined.
   */
  public function getDefaultPluralFormula($lang_code) {
    $default = 'n==1 ? 0 : 1';
    $static_lang_rules = $this->getStaticLanguageRules();
    return $static_lang_rules[$lang_code] ?? $default;
  }

  /**
   * Get the correct rule from config or return the static one.
   */
  public function getRuleById($lang_code) {
    return $this->getRulesFromConfig($lang_code) ?: $this->getDefaultPluralFormula($lang_code);
  }

}
