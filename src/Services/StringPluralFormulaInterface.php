<?php

namespace Drupal\string\Services;

/**
 * Return the rule of the given language id.
 *
 * @package Drupal\string\Services
 */
interface StringPluralFormulaInterface {

  /**
   * {@inheritdoc}
   */
  public function getStaticLanguageRules();

  /**
   * {@inheritdoc}
   */
  public function getRuleById($lang_code);

}
