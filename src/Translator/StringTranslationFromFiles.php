<?php

namespace Drupal\string\Translator;

use Drupal\Component\Gettext\PoMemoryWriter;
use Drupal\Component\Gettext\PoStreamReader;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\Translator\FileTranslation;

/**
 * Provides translation based on po files from a specific directory.
 *
 * ATTENTION: This is a work in progress.
 *
 * This is very much similar to what core does with po files.
 *
 * The idea behind this translator is to support automatic translation sync
 * using services like zanata where the translation source, i.e. "pot" files
 * can be exported to a particular directory and translations i.e. "po" files
 * can be imported to Drupal again after a sync job is run.
 */
class StringTranslationFromFiles extends FileTranslation {

  const PLURAL_MOCK_ID = '';

  /**
   * Constructs a StaticTranslation object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(FileSystemInterface $file_system = NULL) {
    // @todo Discuss where should we keep this.
    $directory = 'public://';
    parent::__construct($directory, $file_system);
  }

  /**
   * Provides translation file name pattern.
   *
   * @param string $langcode
   *   (optional) The language code corresponding to the language for which we
   *   want to find translation files.
   *
   * @return string
   *   String file pattern.
   */
  protected function getTranslationFilesPattern($langcode = NULL) {
    // The file name matches: [language code].po
    // When provided the $langcode is use as language code. If not provided all
    // language codes will match.
    // @todo Allow Prefixes.
    return '!' . $langcode . '\.po$!';
  }

  /**
   * {@inheritdoc}
   */
  protected function getLanguage($langcode) {
    // If the given langcode was selected, there should be at least one .po
    // file with its name in the pattern drupal-$version.$langcode.po.
    // This might or might not be the entire filename. It is also possible
    // that multiple files end with the same suffix, even if unlikely.
    $files = $this->findTranslationFiles($langcode);

    if (!empty($files)) {
      return $this->filesToArray($langcode, $files);
    }
    else {
      return [];
    }
  }

  /**
   * Reads the given Gettext PO files into a data structure.
   *
   * @param string $langcode
   *   Language code string.
   * @param array $files
   *   List of file objects with URI properties pointing to read.
   *
   * @return array
   *   Structured array as produced by a PoMemoryWriter.
   *
   * @see \Drupal\Component\Gettext\PoMemoryWriter
   */
  public static function filesToArray($langcode, array $files) {
    $writer = new PoMemoryWriter();
    $writer->setLangcode($langcode);
    foreach ($files as $file) {
      $reader = new PoStreamReader();
      $reader->setURI($file->uri);
      $reader->setLangcode($langcode);
      $reader->open();
      $writer->writeItems($reader, -1);
    }
    return $writer->getData();
  }

}
