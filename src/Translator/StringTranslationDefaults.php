<?php

namespace Drupal\string\Translator;

use Drupal\Core\StringTranslation\Translator\StaticTranslation;
use Drupal\string\StringManager;

/**
 * Provides default values for strings.
 *
 * The purpose of this translator is to provide default translation when
 * no other method provides any. The default is picked up from the string
 * plugin item defined in my_module.string.yml.
 *
 * E.g. Consider the following item inside my_module.string.yml,
 * ```
 * # Demo: String with a placeholder.
 * dashboard.welcome_message.short:
 *   default: "Hello @name!"
 * ```
 * Then, the default string for `dashboard.welcome_message.short` would be
 * `Hello @name!`.
 *
 * The priority of this translator is kept low so that, any overrides provided
 * via po files or interface translation would take precendence.
 */
class StringTranslationDefaults extends StaticTranslation {

  /**
   * The string service.
   *
   * @var \Drupal\string\StringManager
   */
  protected $stringManager;

  /**
   * Constructs a StaticTranslation object.
   *
   * @param \Drupal\string\StringManager $stringManager
   *   The string manager service.
   */
  public function __construct(StringManager $stringManager) {
    $this->stringManager = $stringManager;
    $translations = $this->getDefaultValues();
    parent::__construct($translations);
  }

  /**
   * Go through each string plugin items and extract default values.
   *
   * @return array
   *   Array of translations defined in string plugin.
   */
  protected function getDefaultValues() {
    $definitions = $this->stringManager->getDefinitions();
    $translations = [];
    foreach ($definitions as $definition) {
      $default_fallback_value = $definition[StringManager::DEFAULT_VALUE] ?? NULL;
      if ($default_fallback_value) {
        $context = $definition[StringManager::MSG_CONTEXT] ?? '';
        $translations[$definition[StringManager::STRING_ID]][$context] = $default_fallback_value;
        // For string overrides.
        $msg_id = $definition[StringManager::MSG_ID] ?? NULL;
        if ($msg_id) {
          $translations[$msg_id][$context] = $default_fallback_value;
        }
      }
    }
    return $translations;
  }

  /**
   * {@inheritdoc}
   */
  public function getStringTranslation($langcode, $string, $context) {
    return $this->translations[$string][$context] ?? FALSE;
  }

}
