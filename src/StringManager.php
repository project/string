<?php

namespace Drupal\string;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Drupal\locale\StringStorageInterface;

/**
 * StringManager class definition.
 */
class StringManager extends DefaultPluginManager implements StringManagerInterface {

  const MSG_ID = 'msgid';
  const MSG_ID_PLURAL = 'msgid_plural';
  const MSG_CONTEXT = 'msgctxt';

  const ID = 'id';
  const STRING_ID = 'sid';
  const LOCALE_ID = 'lid';
  const DEFAULT_VALUE = 'default';
  const DEFAULT_VALUE_PLURAL = 'default_plural';
  const TITLE = 'title';
  const DESCRIPTION = 'description';
  const COMMENTS = 'comments';

  const __DEPRECATED__PLURAL_FORM = 'plural_form';

  /**
   * The local storage service.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  protected $localStorage;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Constructs a new BananaDashboardManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\locale\StringStorageInterface $local_storage
   *   Locale string storage.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *     The theme handler.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend, StringStorageInterface $local_storage, ThemeHandlerInterface $theme_handler) {
    // Add more services as required.
    $this->moduleHandler = $module_handler;
    $this->themeHandler = $theme_handler;
    $this->localStorage = $local_storage;
    $this->setCacheBackend($cache_backend, 'string', ['string']);
    parent::__construct(FALSE, $namespaces, $module_handler);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!isset($this->discovery)) {
      $this->discovery = new YamlDiscovery('string', $this->moduleHandler->getModuleDirectories() + $this->themeHandler->getThemeDirectories());
      // $this->discovery->addTranslatableProperty('string', 'string_context');
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($this->discovery);
    }
    return $this->discovery;
  }

  /**
   * Check if the identifier uses proper namespace.key convention.
   */
  protected function checkForNamespace($id) {
    $splits = explode('.', $id);
    if (count($splits) < 2) {
      throw new PluginException("Plugin 'id' must contain format 'namespace.key'. The id '{$id}' is invalid");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);
    if (empty($definition[StringManager::ID])) {
      throw new PluginException('Plugin property "id" is required for string plugin definition.');
    }
    $this->checkForNamespace($definition[StringManager::ID]);
    $definition[StringManager::STRING_ID] = $definition[StringManager::ID];
    // Create local.storage if not available.
    $source = $definition[StringManager::STRING_ID];
    $context = $definition[StringManager::MSG_CONTEXT] ?? '';
    // Look up the source string and any existing translation.
    $strings = $this->localStorage->getTranslations([
      'source' => $source,
      'context' => $context,
    ]);
    $string = reset($strings);
    $location = (string) $definition['provider'];
    if (empty($string)) {
      $string = $this
        ->localStorage
        ->createString(
          [
            'source' => $source,
            'context' => $context,
          ]
        )
        ->addLocation('string', $location)
        ->save();
    }
    $definition[StringManager::LOCALE_ID] = $string->getId();
    $definition['getLocations'] = $string->getLocations();
  }

  /**
   * Finds plugin definitions.
   *
   * @return array
   *   List of definitions to store in cache.
   */
  protected function findDefinitions() {
    $definitions = $this->getDiscovery()->getDefinitions();
    foreach ($definitions as $plugin_id => &$definition) {
      $this->processDefinition($definition, $plugin_id);
    }
    $this->alterDefinitions($definitions);
    // If this plugin was provided by a module that does not exist, remove the
    // plugin definition.
    foreach ($definitions as $plugin_id => $plugin_definition) {
      $provider = $this->extractProviderFromDefinition($plugin_definition);
      if ($provider && !in_array($provider, ['core', 'component']) && !$this->providerExists($provider)) {
        unset($definitions[$plugin_id]);
      }
    }
    return $definitions;
  }

}
