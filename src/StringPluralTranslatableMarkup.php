<?php

namespace Drupal\string;

use Drupal\Core\StringTranslation\PluralTranslatableMarkup;

/**
 * Translatable plural strings class.
 *
 * @package Drupal\string
 */
class StringPluralTranslatableMarkup extends PluralTranslatableMarkup {

  /**
   * Gets the plural index through the gettext formula.
   *
   * @return int
   *   Index for the string.
   */
  protected function getPluralIndex() {
    $item = [
      [
        '#type' => 'inline_template',
        '#template' => '{{ ' . $this->getRule() . ' }}',
        '#context' => [
          'n' => $this->count,
        ],
      ],
    ];
    // @todo Interpret formula based on language.
    // Find a better way to solve this, however, php's eval is not an option.
    $index = \Drupal::service('renderer')->renderPlain($item);
    return (string) $index;
  }

  /**
   * Get the language rule.
   *
   * @return string
   *   The rule for the langauge.
   */
  protected function getRule() {
    $rule_service = \Drupal::service('string.plural_formula');
    $rule = $rule_service->getRuleById($this->getOption('langcode'));
    $rule = str_replace('&&', 'and', $rule);
    $rule = str_replace('||', 'or', $rule);
    return $rule;
  }

}
