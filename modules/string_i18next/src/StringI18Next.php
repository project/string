<?php

namespace Drupal\string_i18next;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\locale\StringStorageInterface;
use Drupal\string\StringManager;

/**
 * StringI18Next class definition.
 */
class StringI18Next {

  use StringTranslationTrait;

  /**
   * The string manager service.
   *
   * @var \Drupal\string\StringManagerInterface
   */
  protected $stringManager;

  /**
   * The local storage service.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  protected $localStorage;

  /**
   * The constructor.
   */
  public function __construct(StringManager $string_manager, StringStorageInterface $local_storage) {
    $this->stringManager = $string_manager;
    $this->localStorage = $local_storage;
  }

  /**
   * Get strings for a given language code.
   */
  public function getStrings($language_code) {
    $definitions = $this->stringManager->getDefinitions();
    $output = [];
    foreach ($definitions as $key => $value) {
      $key = $this->formatKeyForI18NextItem($key, $value);
      $items = explode('.', $key);
      $leaf = NULL;
      foreach ($items as $k) {
        if ($leaf !== NULL) {

          $leaf = &$leaf[$k];
        }
        else {
          $leaf = &$output[$k];
        }
        $leaf = $leaf ?? [];
      }

      $leaf = $this->convertPluginItemToI18NextItem($value, $language_code);
      unset($leaf);
    }
    return $output;
  }

  /**
   * Create keys compatible with i18next.
   */
  protected function formatKeyForI18NextItem($key, $value) {
    $key = str_replace(':', '.', $key);
    if ($value['msgctxt']) {
      $key .= '_' . str_replace(' ', '_', $value['msgctxt']);
    }
    return $key;
  }

  /**
   * Convert string definition to i18next friendly definition.
   */
  protected function convertPluginItemToI18NextItem($value, $language_code) {
    $output = [];
    $context = $value['msgctxt'] ?? '';
    $placeholders = [
      '@count' => '{{count}}',
    ];
    $options = [
      'context' => $context,
    ];
    if ($language_code !== 'automatic') {
      $options['langcode'] = $language_code;
    }
    if ($value['placeholders']) {
      foreach ($value['placeholders'] as $placeholder) {
        $placeholder_key = $placeholder['key'];
        // Convert '@placeholder' => '{{placeholder}}'.
        $placeholders[$placeholder_key] = '{{' . substr($placeholder_key, '1') . '}}';
      }
    }
    // phpcs:ignore Drupal.Semantics.FunctionT
    $output['string'] = $this->t($value['id'], $placeholders, $options);
    return $output;
  }

}
