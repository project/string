<?php

namespace Drupal\string_tmgmt;

use Drupal\tmgmt_locale\LocaleSourcePluginUi;

/**
 * Locale source plugin UI.
 *
 * Plugin UI for i18n strings translation jobs.
 */
class StringSourcePluginUi extends LocaleSourcePluginUi {

  /**
   * Gets locale strings.
   *
   * @param string $search_label
   *   Label to search for.
   * @param string $missing_target_language
   *   Missing translation language.
   * @param string $context
   *   The translation context.
   *
   * @return array
   *   List of i18n strings data.
   */
  public function getStrings($search_label = NULL, $missing_target_language = NULL, $context = NULL) {
    $langcodes = array_keys(\Drupal::languageManager()->getLanguages());
    $languages = array_combine($langcodes, $langcodes);
    $select = \Drupal::database()
      ->select('locales_source', 'ls')
      ->fields('ls', ['lid', 'source', 'context']);
    $select
      ->addJoin('INNER', 'locales_location', 'location', '[ls].[lid] = [location].[sid]');
    $select->condition('location.type', 'string');
    if (!empty($search_label)) {
      $select->condition('ls.source', '%' . \Drupal::database()->escapeLike($search_label) . '%', 'LIKE');
    }
    if (!empty($context)) {
      $select->condition('ls.context', $context);
    }
    if (!empty($missing_target_language) && in_array($missing_target_language, $languages)) {
      $select->isNull("lt_$missing_target_language.language");
    }

    // Join locale targets for each language.
    // We want all joined fields to be named as langcodes, but langcodes could
    // contain hyphens in their names, which is not allowed by the most database
    // engines. So we create a langcode-to-filed_alias map, and rename fields
    // later.
    $langcode_to_filed_alias_map = [];
    foreach ($languages as $langcode) {
      $table_alias = $select->leftJoin('locales_target', \Drupal::database()->escapeTable("lt_$langcode"), "ls.lid = %alias.lid AND %alias.language = '$langcode'");
      $langcode_to_filed_alias_map[$langcode] = $select->addField($table_alias, 'language');
    }
    unset($field_alias);

    $rows = $select
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(\Drupal::config('tmgmt.settings')->get('source_list_limit', 20))
      ->execute()
      ->fetchAll();
    foreach ($rows as $row) {
      foreach ($langcode_to_filed_alias_map as $langcode => $field_alias) {
        $row->{$langcode} = $row->{$field_alias};
        unset($row->{$field_alias});
      }
    }
    unset($row);
    return $rows;
  }

}
