<?php

namespace Drupal\string_tmgmt\Plugin\tmgmt\Source;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt_locale\Plugin\tmgmt\Source\LocaleSource;

/**
 * Translation Source plugin for locale strings.
 *
 * @SourcePlugin(
 *   id = "string",
 *   label = @Translation("String"),
 *   description = @Translation("Source handler for 'string' strings."),
 *   ui = "Drupal\string_tmgmt\StringSourcePluginUi"
 * )
 */
class StringSource extends LocaleSource {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getItemTypes() {
    return ['default' => $this->t('String')];
  }

}
