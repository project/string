<?php

namespace Drupal\string_demo\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * The StringController class provides test output.
 *
 * @package Drupal\string\Controller
 */
class StringDemoController extends ControllerBase {

  /**
   * Content callback.
   */
  public function content() {
    // Test configuration.
    $languages_being_tested = [
      'en',
      'ru',
      'fr',
      'de',
    ];
    $plural_numbers = [1, 2, 3, 5, 10, 21, 100];

    // Generate Test Output.
    $header = [
      'string id',
      'function',
      '@count',
      '@name',
      'langcode',
      'output',
    ];
    $rows = [];
    $this->prepareVariations($rows, 'Content', $languages_being_tested);
    $this->prepareVariations($rows, 'Pages', $languages_being_tested);
    $this->prepareVariations($rows, 'drupal.core.node.content.label', $languages_being_tested);
    $this->prepareVariations($rows, 'dashboard.welcome_message.short', $languages_being_tested);
    $this->preparePluralVariations($rows, 'search.result.items_count', $languages_being_tested, $plural_numbers);

    $output = [
      '#title' => 'Test strings in various language',
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $output;
  }

  /**
   * Prepare various cases for testing string.
   */
  protected function prepareVariations(array &$rows, $string_id, array $languages_being_tested) {
    $string_test = [
      'id' => $string_id,
      'params' => [
        '@count' => 1,
        '@name' => 'Faker',
      ],
    ];
    foreach ($languages_being_tested as $langcode) {
      foreach ([''] as $count) {
        $string_test['params']['@count'] = $count;
        $row = [
          $string_test['id'],
          't',
          $string_test['params']['@count'],
          $string_test['params']['@name'],
          $langcode,
          // phpcs:ignore Drupal.Semantics.FunctionT -- Used for testing.
          $this->t($string_test['id'], $string_test['params'], ['langcode' => $langcode]),
        ];
        $rows[] = $row;
      }
    }
  }

  /**
   * Prepare various cases for testing complex plural forms.
   */
  protected function preparePluralVariations(array &$rows, $string_id, array $languages_being_tested, array $plural_numbers) {
    $string_test = [
      'id' => $string_id,
      'params' => [
        '@count' => 1,
        '@name' => 'Faker',
      ],
    ];
    foreach ($plural_numbers as $count) {
      foreach ($languages_being_tested as $langcode) {
        $string_test['params']['@count'] = $count;
        $row = [
          $string_test['id'],
          'formatPlural',
          $string_test['params']['@count'],
          $string_test['params']['@name'],
          $langcode,
          $this->formatPlural($count, $string_test['id'], $string_test['id'], $string_test['params'], ['langcode' => $langcode]),
        ];
        $rows[] = $row;
      }
    }
  }

}
